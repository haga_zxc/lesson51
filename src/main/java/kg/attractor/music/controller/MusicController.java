package kg.attractor.music.controller;


import kg.attractor.music.model.Album;
import kg.attractor.music.model.AlbumRepository;
import kg.attractor.music.model.Song;
import kg.attractor.music.model.SongRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
public class MusicController {
    private static final Logger logger = LoggerFactory.getLogger(MusicController.class);
    private AlbumRepository albumRepository;
    private SongRepository songRepository;


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)     //поиск альбома по айди
    public Album getAlbumById(@PathVariable String id) {
        logger.info("Getting album " + id);
        return albumRepository.findById(id).orElse(null);
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)  //поиск альбома по имени
    public Album getAlbumByName(@PathVariable String name) {
        logger.info("Getting album " + name);
        return albumRepository.findByName(name).orElse(null);
    }

    @RequestMapping(value = "/{artist}", method = RequestMethod.GET)  //поиск альбома по исполнителю
    public Album getAlbumByArtist(@PathVariable String artist) {
        logger.info("Getting album " + artist);
        return albumRepository.findByArtist(artist).orElse(null);
    }

    @RequestMapping(value = "/{year}", method = RequestMethod.GET)  // поиск альбома по году выпуска
    public Album getAlbumByYear(@PathVariable LocalDate year) {
        logger.info("Getting album " + year);
        return albumRepository.findByYear(year).orElse(null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)    // поиск песни по айди
    public Song getSongById(@PathVariable String id) {
        logger.info("Getting song " + id);
        return songRepository.findById(id).orElse(null);
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)      // поиск пести по названию
    public Song getSongByName(@PathVariable String name) {
        logger.info("Getting song " + name);
        return songRepository.findByName(name).orElse(null);
    }

    @RequestMapping(value = "/{author}", method = RequestMethod.GET)    // поиск песни по исполнителю
    public Song getSongByAuthor(@PathVariable String author) {
        logger.info("Getting song " + author);
        return songRepository.findByAuthor(author).orElse(null);
    }

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)        //  поиск песни по названию
    public List<Song> getSongsFromAlbum(@PathVariable String name) {
        logger.info("Getting songs from album " + name);
        return albumRepository.getSongsFromAlbum(name);
    }

}
