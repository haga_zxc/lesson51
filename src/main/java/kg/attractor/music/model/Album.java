package kg.attractor.music.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Document
@Data
public class Album {
    @Id
    private String id;
    @Indexed
    private String artist;                     //чтоб легче находить альбом по артисту
    @Indexed
    private String albumName;
    private LocalDate year;

    @DBRef
    private List<Song> albumSongs = new ArrayList<>(); //связь с пемнями их этого альбома


    public Album(String id, String albumName, LocalDate year, List<Song> albumSongs) {
        this.id = id;
        this.albumName = albumName;
        this.year = year;
        this.albumSongs = albumSongs;
    }
}
