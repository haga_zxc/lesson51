package kg.attractor.music.model;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface SongRepository extends CrudRepository<Song, String> {
    Optional<Song> findByName(String name);

    Optional<Song> findByAuthor(String author);


}
