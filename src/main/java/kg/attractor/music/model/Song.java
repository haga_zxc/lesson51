package kg.attractor.music.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Document
public class Song {
    @Id
    private String id;
    @Indexed
    private String songName;
    private String author;

}

