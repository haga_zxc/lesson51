package kg.attractor.music.model;

import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface AlbumRepository extends CrudRepository<Album, String> {
    Optional<Album> findByArtist(String artist);

    Optional<Album> findByName(String name);

    Optional<Album> findByYear(LocalDate date);

    List<Song> getSongsFromAlbum(String name);


}
